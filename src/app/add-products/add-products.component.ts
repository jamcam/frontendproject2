import { Component, OnInit } from '@angular/core';
import { AddProductsService } from '../add-products.service';
import { Product } from '../products';
import { RegisterService } from '../register.service';


@Component({
  selector: 'app-add-products',
  templateUrl: './add-products.component.html',
  styleUrls: ['./add-products.component.css']
})
export class AddProductsComponent implements OnInit {
  id!:number;
  url:string = "";
  name:string = "";
  price!:number;
  description:string = "";
  products!: Product;
  items = this.addproductsService.getItems();

  constructor(private addproductsService: AddProductsService, public registerService:RegisterService) { }

  addProducts(){
    this.products = {url:this.url, name:this.name, price:this.price, description:this.description, id:this.id };
    this.addproductsService.addProducts(this.products);
    window.alert('The product has been created!');
    window.alert(JSON.stringify(this.products));
    
  }

  addProductstoDB(){
    const Formdata = {url: this.url, productName: this.name, productPrice: this.price, description: this.description};
    this.registerService.addProduct(Formdata).subscribe(data => {
      console.log("POST request was successful ", data);});
  }



  ngOnInit(): void {
  }

}
