import { Component, OnInit } from '@angular/core';
import { Product, products } from '../products';
import { CartService } from '../cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {
  products = products;
  items = this.cartService.getItems();

  constructor(private cartService: CartService) { 

  }

 

  ngOnInit(): void {
  }

}

