export interface Product {
    id: number;
    name: string;
    price: number;
    description: string;
    url: string;
  }
  
  export const products = [
    {
      id: 1,
      url: "https://images.samsung.com/uk/smartphones/galaxy-s21/buy/02_ImageCarousel/01_FamilyShot/S21_Family_KV_MO_img.jpg",
      name: 'Samsung Galaxy S21+ Plus',
      price: 999.99,
      description: "With a sleek new design, gorgeous 6.7-inch Infinity-O Display with adaptive refresh rate, and a battery that won't give out on you when you need it most, Samsung Galaxy S21+ Plus 5G phone lets you get creative, stay connected, and do it all your way. Life moves fast, and there’s a fine line between what takes a moment from ordinary to extraordinary. That’s why we created the S21+ Plus. With the ability to record in 8K, your videos will be cinema quality. Use the 64MP high-resolution camera for still shots that come out crystal-clear whether it’s day or night. Single Take AI transcends the usual restrictions of photo and video editing to capture life’s greatest moments, wherever they happen, in one single take, capturing images and video for up to 15 seconds. Innovative and beautifully designed, Samsung 5G phones emphasize the brand’s dedication to the latest cutting-edge technology and next-generation connectivity. Enjoy more sharing, more streaming, more experiences, and more connections at hyperfast speed. The Galaxy S21+ 5G does not currently support eSim in the U.S. Battery power consumption depends on usage patterns and results may vary. 5G speeds vary and require optimal network and connection (factors include frequency, bandwidth, congestion)."
    },
    {
      id: 2,
      url: "https://store.storeimages.cdn-apple.com/4982/as-images.apple.com/is/iphone-13-mini-pink-select-2021?wid=940&hei=1112&fmt=png-alpha&.v=1629842710000",
      name: 'Phone Mini',
      price: 699,
      description: 'A great phone with one of the best cameras'
    },
    {
      id: 3,
      url: "https://m.media-amazon.com/images/I/81dUzXzVIPS._AC_SX355_.jpg",
      name: 'SAMSUNG M5 Series',
      price: 239.99,
      description: 'Do-It-All Screen for every side of life. Get work done without a PC, with the installed Microsoft Office 365, or by remote access to your office computer. Then switch to pure entertainment with the on-board one-stop entertainment system.'
    }
  ];