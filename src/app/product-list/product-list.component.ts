import { Component, OnInit } from '@angular/core';
import { Product, products } from '../products';
import { CartService } from '../cart.service';
import { AddProductsService } from '../add-products.service';

@Component({
  selector: 'app-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.css']
})
export class ProductListComponent implements OnInit {
  products = products;
  badgeContent!: number;
  badgeCounter!: number;
  items = this.addproductsService.items;

  constructor(private cartService: CartService, private addproductsService: AddProductsService) { 
    this.badgeContent = 0;
    this.badgeCounter = 0;
  }

  incrementCount() {
    this.badgeContent++;
  }

  addToCart(product: Product) {
    this.cartService.addToCart(product);
    window.alert('Your product has been added to the cart!');
  }

  ngOnInit(): void {
  }

}
