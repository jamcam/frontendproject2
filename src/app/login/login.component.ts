import { Component, OnInit } from '@angular/core';
import { RegisterService } from '../register.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  password: string ='';
  email: string ='';

  constructor(private router: Router, public registerService:RegisterService) { }

  ngOnInit(): void {
  }

  loginUser(){
    if(this.password != "" && this.email != ""){
      const Formdata = {email: this.email, password: this.password}
      this.registerService.getUser(Formdata).subscribe(data => {
        console.log("POST request was successful "); if(data == true){this.router.navigate(['/products'])}else if(data == null){alert("User does not exist!")}else{alert("Wrong email/password combination. Try again!")}});
    }
  }

}
