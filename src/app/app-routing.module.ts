import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import {CartComponent} from './cart/cart.component';
import { ProductListComponent } from './product-list/product-list.component';
import { AddProductsComponent } from './add-products/add-products.component';

const routes: Routes = [
  { path: '', component: RegisterComponent},
  { path: 'login', component: LoginComponent},
  { path: 'cart', component: CartComponent},
  { path: 'products', component: ProductListComponent},
  { path: 'addproducts', component: AddProductsComponent}
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
})
export class AppRoutingModule { }
