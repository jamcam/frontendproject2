import { Injectable } from '@angular/core';
import { Product } from './products';

@Injectable({
  providedIn: 'root'
})
export class AddProductsService {
  items: Product[] = [];

  addProducts(product: Product){
    this.items.push(product);
    
  }

  getItems() {
    return this.items;
  }

  constructor() { }
}
