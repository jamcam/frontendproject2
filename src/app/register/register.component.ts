import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  firstname: string = '';
  lastname: string = '';
  password: string ='';
  email: string ='';
  
  constructor(private router: Router, public registerService:RegisterService) { }

  ngOnInit(): void {
  }

  registerUser(){
    if(this.firstname != "" && this.lastname != "" && this.password != "" && this.email != ""){
      alert("You're registered!")
      
        this.router.navigate(['/login'])

        const Formdata = {firstName: this.firstname, lastName: this.lastname, password: this.password, email: this.email}

        this.registerService.addUser(Formdata).subscribe(data => {
          console.log("POST request was successful ", data)});

    }

        
  }
    
}
