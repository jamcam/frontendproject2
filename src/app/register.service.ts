import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from  '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  baseURL: string = "http://localhost:8080/project2/user";

  

  constructor(private http: HttpClient) { }

  addUser(data: any): Observable<any> {
    return this.http.post(this.baseURL, data)
  }

  getUser(data: any): Observable<any> {
    return this.http.post("http://localhost:8080/project2/validateuser", data)
  }

  addProduct(data: any): Observable<any> {
    return this.http.post("http://localhost:8080/project2/product", data)
  }
  
}